# Taquin #

Taquin est une application console réalisée en deuxième année de BTS SIO.

## But de l'application ##

Cette application est un jeu où il faut placer dans l'ordre les chiffres de 0 à 8 en déplaçant le 0.

### Les méthodes ###


public override string ToString()

public bool Jouer(int unCoup)

public bool Gagné()

public void NouveauJeu()

public void tableauTest()

public int GetLigneVide

public int GetColonneVide

int this[int ligne, int colonne]


### Le code de la méthode Jouer ###


```
#!c#
public bool Jouer(int unCoup)
		{
			bool coupJoué = true;
            // A compléter

			if (unCoup==2) 
			{
                if (ligneVide  > 0 )
                {
                    int vb1;
                    vb1 = plateau[ligneVide - 1, colonneVide];
                    plateau[ligneVide - 1, colonneVide] = plateau[ligneVide, colonneVide];
                    plateau[ligneVide, colonneVide] = vb1;

                    ligneVide = ligneVide - 1;
                }
                
			}
			else
			{
				if (unCoup==4)
				{
                    if (colonneVide < 2)
                    {
                        int vb1;
                        vb1 = plateau[ligneVide, colonneVide + 1];
                        plateau[ligneVide, colonneVide + 1] = plateau[ligneVide, colonneVide];
                        plateau[ligneVide, colonneVide] = vb1;

                        colonneVide = colonneVide + 1;
                    }                  
				}
				else
				{
					if (unCoup==6)
					{
                        if (colonneVide > 0)
                        {
                            int vb1;
                            vb1 = plateau[ligneVide, colonneVide - 1];
                            plateau[ligneVide, colonneVide - 1] = plateau[ligneVide, colonneVide];
                            plateau[ligneVide, colonneVide] = vb1;

                            colonneVide = colonneVide - 1;
                        }    
					}
					else
					{
						if(unCoup==8)
						{
                            if (ligneVide < 2 )
                            {
                                int vb1;
                                vb1 = plateau[ligneVide + 1, colonneVide];
                                plateau[ligneVide + 1, colonneVide] = plateau[ligneVide, colonneVide];
                                plateau[ligneVide, colonneVide] = vb1;

                                ligneVide = ligneVide + 1;    
                            }   
						}
						else
						{
                            // le joueur n'a pas choisi 2,4,6 ou 8, il n'a pas joué
							coupJoué=false;
						}
					}

				}
			}
			return coupJoué;
		}

```